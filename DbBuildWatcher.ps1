$SourcePath="\\192.168.77.7\transfer";
$LocalPath="D:\AgencyTeam\DB_Bak";
$BuildWatcherFile="D:\AgencyTeam\DB_Bak\ok.txt";
$LogFile="D:\AgencyTeam\DB_Bak\buildWatcher.txt";
Import-Module BitsTransfer;
function Get-Dbversion {
    param(
        # Parameter help description
        [Parameter(Mandatory=$true)]
        [string]
        $DbFileName
    )
    $revisionNumber = $DbFileName.Split("_")[-1].Replace(".BAK","");
    return $revisionNumber;    
}

function Get-AgencyDBFileList {
    param(
        # Parameter help description
        [Parameter(Mandatory=$false)]
        [string]
        $SearchPattern="NJCortland.Agency.DAL.AgencyContext_db_",

        # Parameter help description
        [Parameter(Mandatory=$false)]
        [string]
        $FolderPath=$SourcePath
    )
    $FileList = Get-ChildItem -Path $FolderPath | ?{$_.Name.Contains($SearchPattern)} |Select-Object -Property Name,FullName;
    return $FileList;    
}
function Get-LatestBuild  {
    param(
        # Parameter help description
        [Parameter(Mandatory=$false)]
        [array]
        $BuildList  
    )
    return $BuildList | Sort-Object -Property Name | Select-Object -Last 1
}

function Get-CurrentBuild  {
    param(
        # Parameter help description
        [Parameter(Mandatory=$false)]
        [string]
        $BuildVersionPath=$BuildWatcherFile
    )
    if(Test-Path $BuildVersionPath){
        return Get-Content $BuildVersionPath;
    }
    else {
        ""|Out-File $BuildVersionPath;
        $BuildFile = Get-Item $BuildVersionPath;
        $ParrentFolder = $BuildFile.Directory.FullName;
        $BuildList = Get-AgencyDBFileList -FolderPath $ParrentFolder 
        $LatestBuild = Get-LatestBuild $BuildList
        Get-Dbversion $LatestBuild.Name | Out-File $BuildVersionPath
        return Get-Content $BuildVersionPath;
    }
}
function Copy-File {
    param( [string]$from, [string]$to)
    $ffile = [io.file]::OpenRead($from)
    $tofile = [io.file]::OpenWrite($to)
    Write-Progress -Activity "Copying file" -status "$from -> $to" -PercentComplete 0
    try {
        [byte[]]$buff = new-object byte[] 4096
        [int]$total = [int]$count = 0
        do {
            $count = $ffile.Read($buff, 0, $buff.Length)
            $tofile.Write($buff, 0, $count)
            $total += $count
            if ($total % 1mb -eq 0) {
                Write-Progress -Activity "Copying file" -status "$from -> $to" `
                   -PercentComplete ([int]($total/$ffile.Length* 100))
            }
        } while ($count -gt 0)
    }
    finally {
        $ffile.Dispose()
        $tofile.Dispose()
        Write-Progress -Activity "Copying file" -Status "Ready" -Completed
    }
}

function Write-Log  {
    param([string]$log)
    Write-Host "$log Logged at $(Get-Date)"
    "$log Logged at $(Get-Date)" | Out-File -Append $LogFile 
}

function copy-LargeFile {
    param(
         [string]$src, [string]$dest
    )
    $displayName = "MyBitsTransfer " + (Get-Date)            
    Start-BitsTransfer `
        -Source $src `
        -Destination $dest `
        -DisplayName $displayName `
        -Asynchronous            
    $job = Get-BitsTransfer $displayName   
    $lastStatus = $job.JobState            
    Do {            
        If ($lastStatus -ne $job.JobState) {            
            $lastStatus = $job.JobState  
        }            
        If ($lastStatus -like "*Error*") {            
            Remove-BitsTransfer $job            
            Write-Log "Error connecting to download."      
            return;            
        }            
    }            
    while ($lastStatus -ne "Transferring")

    do {            
        Write-Log "$(Get-Date) $($job.BytesTransferred) $($job.BytesTotal) `
            $($job.BytesTransferred/$job.BytesTotal*100)"            
        Start-Sleep -s 120            
    }            
    while ($job.BytesTransferred -lt $job.BytesTotal)
    Write-Log "$(Get-Date) $($job.BytesTransferred) $($job.BytesTotal) `
            $($job.BytesTransferred/$job.BytesTotal*100)%"
    Complete-BitsTransfer $job    
}

while (1) {
    $CurrentBuildRevision = Get-CurrentBuild;
    $SourceBuildList = Get-AgencyDBFileList ;
    $LatestBuild = Get-LatestBuild $SourceBuildList;
    $latestBuildRevision = Get-Dbversion $LatestBuild.Name;
    if($CurrentBuildRevision -ne $latestBuildRevision){
        Write-Host "Sleep 5mins"
        "Sleep 5mins at: $(Get-Date)" | Out-File -Append $LogFile
        Start-Sleep 300;
        Write-Host "Sync the Build to the local with name: $($LatestBuild.Name)";
        "Sync the Build to the local with name: $($LatestBuild.Name)" |  Out-File -Append $LogFile
        Write-Host "Start to Sync at $(Get-Date)"
        "Start to Sync at $(Get-Date)" |  Out-File -Append $LogFile
        $StartDate=Get-Date
        $targetPath = Join-Path $LocalPath $LatestBuild.Name;
        #Copy-File $LatestBuild.FullName $targetPath;
        copy-LargeFile $LatestBuild.FullName $targetPath
        $endDate=Get-Date
        $latestBuildRevision | Out-File $BuildWatcherFile
        Write-Host "end Sync at $(Get-Date)"
        "end Sync at $(Get-Date)" | Out-File -Append $LogFile
        Write-Log "Time Consume for DB sync is $(($endDate - $StartDate).Minutes)"
    }
    else {
        Write-Host "No Latest Build Found. Sleep 30 mins";
         "No Latest Build Found. Sleep 30 mins, at $(Get-Date)" | Out-File -Append $LogFile
        Start-Sleep -Seconds 1800;
    }
}

