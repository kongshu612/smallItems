Write-host "Stop the Lync"
gps -name "lync" | stop-process

Write-host "Clean the legacy registry Path about lync"
if(Test-Path HKCU:\SOFTWARE\Microsoft\Office\16.0\Lync\xiangjian.wu@cortlandglobal.com)
{
     Remove-Item HKCU:\SOFTWARE\Microsoft\Office\16.0\Lync\xiangjian.wu@cortlandglobal.com -Recurse -Force
}
Write-host "Clean the tmp folder Path about lync"
if(Test-Path C:\Users\xiangjian.wu\AppData\Local\Microsoft\Office\16.0\Lync\sip_xiangjian.wu@cortlandglobal.com)
{
    remove-Item C:\Users\xiangjian.wu\AppData\Local\Microsoft\Office\16.0\Lync\sip_xiangjian.wu@cortlandglobal.com -Recurse -Force
}
Write-host "Start Lync"
& "C:\Program Files (x86)\Microsoft Office\Root\Office16\lync.exe"