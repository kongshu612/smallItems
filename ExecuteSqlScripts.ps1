Param(
    
    [Parameter(Mandatory=$false)]
    [string[]]$SubFolders=@("StoredProcedures","DatabaseUpdates\v1.4 Stem (201711)"),
    [Parameter(Mandatory=$false)]
    [string]$RepositoryRootPath="D:\AgencyTeam\CI\jobs\Agency_201711_ChinaDev\workspace\default\sql",    
    [Parameter(Mandatory=$false)]
    [string]$SqlInstance="192.168.0.112\agencychinauat",    
    [Parameter(Mandatory=$false)]
    [string]$DataBaseName="Cortland.Agency.DAL.AgencyContext_201711Dev",    
    [Parameter(Mandatory=$false)]
    [string]$UserName="sa",    
    [Parameter(Mandatory=$false)]
    [string]$PassW="Cortland1"
)

Import-Module SqlPs -DisableNameChecking
function EXECScroptFolder {
    Param(
        [Parameter(Mandatory=$false)]
        [string]$SubPath
    )
    Write-Host "$SubPath"
    $FullPath = Join-Path -Path $RepositoryRootPath -ChildPath $SubPath
    $SPFileList = Get-ChildItem -Path $FullPath -Filter "*.sql" -Recurse
    
    $SPFileList | %{
        Write-Host $_.FullName
        invoke-sqlcmd -inputfile $($_.FullName) -serverinstance $SqlInstance -database $DataBaseName -Username $UserName -Password $PassW
    }
}

$SubFolders | %{
    EXECScroptFolder -SubPath $_
}
