param(
    [Parameter(Mandatory=$false)]
    [string]$ReportServerUrL = "http://192.168.0.112:8070/ReportServer/ReportService2010.asmx?wsdl",
   # [string]$ReportServerUrL = "http://localhost/ReportServer/ReportService2010.asmx?wsdl",

    [Parameter(Mandatory=$false)]
    [string]$LocalRDLFilePath = "C:\CodeRepository\ChinaDev-10\Cortland.Agency.BL\Reports\SSRSReports",

    [Parameter(Mandatory=$false)]
    [string]$ReportFolderPath = "/201710Warranty_SSRS",

    [Parameter(Mandatory=$false)]
    [string]$SharedDataSourcePath = "/201710Warranty_SSRS/Agency",

    [Parameter(Mandatory=$false)]
    [validateSet("Initialize","UploadRDL","UpdateDS")]
    [string]$Action = "Initialize"
)
Function Get-ReportServerInstance
 {
     param(

     [Parameter(Mandatory=$true)]
     [string]$ReportUrl,

     [Parameter(Mandatory=$false)]
     [string]$Namespace
     )
 
     return New-WebServiceProxy -Uri $ReportUrl -UseDefaultCredential -Namespace $Namespace
 }

 Function Delete-ItemsByPath
 {
     param(
         [Parameter(Mandatory=$true)]
         [object]$RS,

         [Parameter(Mandatory=$true)]
         [string]$Path,

         [Parameter(Mandatory=$false)]
         [bool]$IsRecurse=$false
     )
     $RS.DeleteItem($Path)
     if($IsRecurse){$RS.ListChildren($Path,$IsRecurse) | %{$RS.DeleteItem($_.Path)}}
 }

 Function Upload-ReportToRemoteServer
 {
     param(
         [Parameter(Mandatory=$true)]
         [object]$RS,

         [Parameter(Mandatory=$true)]
         [string]$RDLFilePath,

         [Parameter(Mandatory=$true)]
         [string]$ReportServerPath
     )
     Resolve-Path -Path $RDLFilePath
     $RDLFile = Get-Item -Path $RDLFilePath
     $reportName = [System.IO.Path]::GetFileNameWithoutExtension($RDLFile.Name)
     $bytes = [System.IO.File]::ReadAllBytes($RDLFile.FullName)
     $warnings=$null
     $report = $rs.CreateCatalogItem(
        "Report",         # Catalog item type
        $reportName,      # Report name
        $ReportServerPath,# Destination folder
        $true,            # Overwrite report if it exists?
        $bytes,           # .rdl file contents
        $null,            # Properties to set.
        [ref]$warnings)   # Warnings that occured while uploading.
    $warnings | %{  Write-Output ("Warning: {0}" -f $_.Message)}
 }

 Function Create-ReportFolder
 {
     Param(
         [Parameter(Mandatory=$true)]
         [object]$RS,

         [Parameter(Mandatory=$true)]
         [string]$FolderName,

         [Parameter(Mandatory=$false)]
         [string]$ParentFolderPath="/"
     )
     $RS.CreateFolder($FolderName,$ParentFolderPath,$null)
 }

 Function Test-ItemByPath
 {
     Param(
         [Parameter(Mandatory=$true)]
         [object]$RS,

         [Parameter(Mandatory=$true)]
         [string]$Path
     )
     Return ($Rs.ListChildren("/",$true) | ?{$_.Path -eq $Path}) -ne $null
 }

 Function Update-DataSource
 {
     Param(
         [Parameter(Mandatory=$true)]
         [object]$RS,

         [Parameter(Mandatory=$true)]
         [string]$SharedDataSourcePath,

         [Parameter(Mandatory=$true)]
         [string]$ReportFolderPath
     )
     $RS.ListChildren($ReportFolderPath,$false) |?{$_.TypeName -eq "Report"} | %{
           $referencedDataSourceName = (@($rs.GetItemReferences($_.Path, "DataSource")))[0].Name
            # Change the datasource for the report to $SharedDataSourcePath
            # Note that we can access the types such as DataSource with the prefix 
            # "SSRS" only because we specified that as our namespace when we 
            # created the proxy with New-WebServiceProxy.
            $dataSource = New-Object SSRS.DataSource
            $dataSource.Name = $referencedDataSourceName      
            # Name as used when designing the Report
            $dataSource.Item = New-Object SSRS.DataSourceReference
            $dataSource.Item.Reference = $SharedDataSourcePath # Path to the shared data source as it is deployed here.
            $rs.SetItemDataSources($_.Path, [SSRS.DataSource[]]$dataSource)            
    }
 }


$Rs = Get-ReportServerInstance -ReportUrl $ReportServerUrL  -Namespace "SSRS"
if($Action -eq "Initialize")
{
    if((Test-ItemByPath -RS $RS -Path $ReportFolderPath) -eq $false)
    {
        Create-ReportFolder -RS $RS -FolderName $ReportFolderPath.remove(0,1)
    }
    Resolve-Path $LocalRDLFilePath
    Get-ChildItem -Path $LocalRDLFilePath | ?{$_.Name -like "*.rdl"} | %{Upload-ReportToRemoteServer -RS $RS -RDLFilePath $_.FullName -ReportServerPath $ReportFolderPath }
    $RS = $null
}
elseif($Action -eq "UpdateDS")
{
    Update-DataSource -RS $RS -SharedDataSourcePath $SharedDataSourcePath -ReportFolderPath $ReportFolderPath
    $RS=$null
}

