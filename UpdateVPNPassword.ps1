$WebUrL = "http://h6v6.com/"
$SSExe = ".\ss\shadowsocks.exe"
$SSConfigPath = ".\ss\gui-config.json"

Function Update-Credential
{
    Param()
    Write-host "Get the VPN Credentials"
Write-host "make a web request to the URL: http://h6v6.com/"
    $WebResults = Invoke-WebRequest $WebUrL
    $tmp = $WebResults.AllElements | ?{$_.tagName -eq "TR"}
    $targetCredential = @($tmp[1].outerHTML)
    $targetCredential += $tmp[2].outerHTML
    $VPNCredentials = @()
    $targetCredential | %{
        $t = [xml]$_.tostring().replace("<TD>&nbsp;</TD>","")
        $VPNCredentials+= @{"ServerIp"=$t.tr.td[1];"Port"=$t.tr.td[2];"Password"=$t.tr.td[3]}
    }
    Write-host "the VPN Credentials are:"
    $VPNCredentials

    Write-host "Insert or Update the Server config"
    $configFileInObject = Get-Content $SSConfigPath | ConvertFrom-Json 
    Foreach($eachVPN in $VPNCredentials)
    {
        $existingConfig = $configFileInObject.configs | ?{$_.server -eq $eachVPN.ServerIp}
        if($existingConfig -ne $null)
        {
            $existingConfig.Password = $eachVPN.Password
        }
    }
    Write-host "The current Config servers are: "
    $configFileInObject.configs
    Write-host "Write the Modification back to file"
    $configFileInObject | ConvertTo-Json | out-file $SSConfigPath 
    Write-host "Restart the shadowsocks"
    gps -name "shadowsocks" | stop-process
    Write-host "Restart ..."
    sleep 10
    & $SSExe
}

cd C:\DocumentNoteRepository\Tools
Write-host "Start ss"
& $SSExe
sleep 10
Write-host "Update at time"
get-date
Update-Credential
$isUpdated = $true

while(1)
{
    if(((get-date).Minute -lt 1) -and ($isUpdated -eq $false))
    {
        Write-host "Update at time"
        get-date
        Update-Credential
        $isUpdated = $true
        sleep -s 60
    }
    else
    {
        sleep -s 60
        $isUpdated = $false
    }
}

